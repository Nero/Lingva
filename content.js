browser.runtime.onMessage.addListener((request) => {
    if (request.name == "getSelection") {
        return Promise.resolve({ response: window.getSelection().toString() });
    }
})