const source = document.getElementById("source")
const switchButton = document.getElementById("switchButton")
const target = document.getElementById("target")
const query = document.getElementById("query")
const translation = document.getElementById("translation");
const copyButton = document.getElementById("copyButton");

// Get Storage
browser.storage.local.get({
	sourceLg: "auto",
	targetLg: "en",
	rowsCount: 10,
	columnsCount: 60
}).then((storage) => {
	source.value = storage.sourceLg;
	target.value = storage.targetLg;
	query.rows = storage.rowsCount;
	translation.rows = storage.rowsCount;
	query.cols = storage.columnsCount;
	translation.cols = storage.columnsCount;
});

let timer;
source.onchange = () => {
	getTranslation();
	browser.storage.local.set({
		sourceLg: source.value
	});
};
target.onchange = () => {
	getTranslation();
	browser.storage.local.set({
		targetLg: target.value
	});
}
query.onkeyup = () => {
	clearTimeout(timer);
	timer = setTimeout(getTranslation, 500)
}
switchButton.onclick = () => {
	if (source.value != "auto") {
		let curSource = source.value;
		source.value = target.value;
		target.value = curSource;
		getTranslation();
		browser.storage.local.set({
			targetLg: target.value,
			sourceLg: source.value
		});
	}
	else {
		source.style.backgroundColor = "#d22";
		setTimeout(() => {
			source.style.backgroundColor = "#333";
		}, 1500);
	}
};
copyButton.onclick = () => {
	navigator.clipboard.writeText(translation.value);
};

// Listen for browser messages by clicking context menu
browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if (request.name == "translateText") {
		query.value = request.selectedText;
		getTranslation();
	}
});

function getTranslation() {
	if (query.value === "") {
		translation.value = "";
	}
	else {
		translation.value = "..."
		browser.storage.local.get({
			instance: "https://translate.plausibility.cloud"
		}).then(async (storage) => {
			const encodedQuery = query.value.replaceAll("/", "$EscapeText$");
			const url = `${storage.instance}/api/v1/${source.value}/${target.value}/${encodedQuery}`;
			(await fetch(url))
				.json()
				.then(data => {
					if (data.translation === undefined) {
						getTranslationFallback();
					} else {
						translation.value = data.translation.replaceAll("$EscapeText$", "/");
					}
				})
		})
	}
}

async function getTranslationFallback() {
	(await fetch("https://libretranslate.de/translate", {
		method: "POST",
		body: JSON.stringify({
			q: query.value,
			source: source.value,
			target: target.value,
			format: "text",
			api_key: ""
		}),
		headers: { "Content-Type": "application/json" }
	})).json().then(res => translation.value = res.translatedText);
}