// Right click menu
browser.menus.create({
	id: "translate-selection",
	title: "Translate '%s'",
	contexts: ["selection"]
});

browser.menus.onClicked.addListener((info, tab) => {
	if (info.menuItemId == "translate-selection") {
		sendMessage(info.selectionText);
	}
});

// Listen for keyboard shortcut
browser.commands.onCommand.addListener((command) => {
	browser.browserAction.openPopup();

	if (command == "translate-selection") {
		browser.tabs.query(
			{
				currentWindow: true,
				active: true
			}
		)
		.then((tabs) => {
			browser.tabs
			.sendMessage(tabs[0].id, { name: "getSelection" })
			.then((response) => {
				setTimeout(() => {
					browser.runtime.sendMessage(
						{
							name: "translateText",
							selectedText: response.response
						}
					);
				}, 300)
			});
		})
	}
});


function sendMessage(text) {
	browser.browserAction.openPopup();
	setTimeout(() => {
		browser.runtime.sendMessage(
			{
				name: "translateText",
				selectedText: text
			}
		);
	}, 500);
}