const instance = document.getElementById("instance");
const defSourceLg = document.getElementById("defSourceLg");
const defTargetLg = document.getElementById("defTargetLg");
const rowsCount = document.getElementById("rowsCount");
const columnsCount = document.getElementById("columnsCount");
const previewTextArea = document.getElementById("previewTextArea");
const saveBtn = document.getElementById("saveBtn");


// Restore options
document.addEventListener("DOMContentLoaded", () => {
    let getting = browser.storage.local.get({
        instance: "https://translate.plausibility.cloud",
        sourceLg: 'auto',
        targetLg: 'en',
        rowsCount: 7,
        columnsCount: 60
    });
    getting.then((storage) => {
        instance.value = storage.instance;
        defSourceLg.value = storage.sourceLg;
        defTargetLg.value = storage.targetLg;
        rowsCount.value = storage.rowsCount;
        columnsCount.value = storage.columnsCount;
        previewTextArea.rows = storage.rowsCount;
        previewTextArea.cols = storage.columnsCount;
    });
});

// Save settings
document.getElementById("settings").addEventListener("submit", (e) => {
    e.preventDefault();

    if (
        !instance.value.trim().length ||
        !rowsCount.value.trim().length ||
        !columnsCount.value.trim().length
    ) {
        alert("Please fill out all fields!");
    } else {
        browser.storage.local.set({
            instance: instance.value,
            sourceLg: defSourceLg.value,
            targetLg: defTargetLg.value,
            rowsCount: rowsCount.value,
            columnsCount: columnsCount.value
        }).then(() => {
            saveBtn.disabled = true;
            saveBtn.innerText = "Saved!";
            setTimeout(() => {
                saveBtn.disabled = false;
                saveBtn.innerText = "Save";
            }, 3000)
        });
    }
})

// Preview of Textarea
rowsCount.addEventListener("change", () => {
    if (rowsCount.value > 0 && rowsCount.value <= 14) {
        previewTextArea.rows = rowsCount.value;
    }
})
columnsCount.addEventListener("change", () => {
    if (columnsCount.value > 24 && columnsCount.value <= 100) {
        previewTextArea.cols = columnsCount.value;
    }
})
